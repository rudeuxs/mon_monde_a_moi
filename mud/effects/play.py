# -*- coding: utf-8 -*-
# Copyright (C) 2014 Radureau Gabriel, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import PlayEvent, PlayWithEvent

class PlayEffect(Effect2):
    EVENT = PlayEvent

class PlayWithEffect(Effect3):
    EVENT = PlayWithEvent
