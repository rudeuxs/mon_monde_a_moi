# -*- coding: utf-8 -*-
# Copyright (C) 2014 Radureau Gabriel, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import PlayEvent, PlayWithEvent


class PlayAction(Action2):
    EVENT = PlayEvent
    ACTION = "play"
    RESOLVE_OBJECT = "resolve_for_operate"


class PlayWithAction(Action3):
    EVENT = PlayWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "play-with"
