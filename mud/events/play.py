# -*- coding: utf-8 -*-
# Copyright (C) 2014 Radureau Gabriel, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3


class PlayEvent(Event2):
    NAME = "play"

    def perform(self):
        if not self.object.has_prop("playable"):
            self.fail()
            return self.inform("play.failed")
        self.inform("play")


class PlayWithEvent(Event3):
    NAME = "play-with"
    
    def perform(self):
        if not self.object.has_prop("shootable"):
            self.fail()
            return self.inform("play-with.failed")
        self.inform("play-with")
